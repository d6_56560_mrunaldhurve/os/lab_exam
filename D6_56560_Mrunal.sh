

function pattern() 
{
        i=0
        while [ $i -le `expr $1 - 1` ]
        do
                j=0
                while [ $j -le `expr $1 - 1` ]
                do
                        if [ `expr $1 - 1` -le `expr $i + $j` ]
                        then    
                                echo -ne "*"
                        else
                                echo -ne " "
                        fi
                        j=`expr $j + 1`
                done
                echo
                i=`expr $i + 1`
        done
}

function strong()
{
if [ $# -eq 1 ]
then
    n=$1
    sum=0
    k=$n

    while [ $n -gt 0 ]
     do
    d=`echo $n%10|bc`
    fact=1
    i=1
    while [ $i -le $d ]

    do

    fact=`echo $fact*$i|bc`

    i=`echo $i+1|bc`

    done

    sum=`echo $sum+$fact|bc`

    n=`echo $n/10|bc`

    done

    if [ $sum -eq $k ]

    then

    echo it is strong number

    else

    echo it is not strong number

    fi
 else
  echo "invalid number of position parameters."
fi

}



while [ true ]
do
clear
# display menu list 
echo "0. exit"
echo "1. print all strong number in given range "
echo "2. pattern"
echo "3. Multiplication of two floating point numbers"
echo "4. Write a command to remove write permissions for a group of a given file."
echo "5. Write a command to convert spaces of a given string to the newline characters"
echo "6. Write a command to find a directory with a name in your home directory."
echo "7. Write a command to count the number of occurrences of a word in the file."

echo -n "enter the choice : "
read choice

if [ $choice -eq 0 ] 
then exit
fi

case $choice in
# Print all Strong numbers between the given range
1)
   echo "enter the number :"
   read number
   strong $number

   ;;
#  Print pattern. (Take the number of rows from the user)
2)
   echo "enter the number of rows: "
   read rows
    pattern $rows


 ;;
# Write a command to print the multiplication of two floating-point numbers.
3) 
  echo -n "enter number 1:"
  read n1
  echo -n "enter number 2:"
  read n2

  res=`echo "$n1*$n2" | bc`
  echo "result is :$res"
  ;;
# Write a command to remove write permissions for a group of a given file.
4)
 echo "Enter the filename : "
                read file
                if [ -e $file ]
                then    
                        if [ -f $file ]
                        then
                                `chmod g-w- $file`  
                        fi 
                fi
                ls -l
                echo "write permission for a group of a given file is removed successfully"
                ;;


#  Write a command to convert spaces of a given string to the newline characters.
5) 
 echo "/1.Mrunal/Raj/Dhurve /2.Mrunal/Raj/Dhurve " | tr " " "\n"
 echo "/path/to/file /path/to/file2 "\
| tr " " "\n"
;;

# Write a command to find a directory with a name in your home directory.
6)
   echo "enter the filename : "
                read file 
                echo "Current file location with a name in home directory : "
                if [ -e $file ]
                then
                        if [ -f $file ]
                        then   
                                echo `pwd`/$file
                        fi 
                fi 
;;
 
 #Write a command to count the number of occurrences of a word in the file.
7) 
          echo "enter the filename : "
                read file
                echo "enter the word to check count of word : "
                read word
                grep -o -i $word $file | wc -l
;;

*)
   echo "invalid choice"
   ;;
   esac

echo "press enter to continue"
  read enter

done
